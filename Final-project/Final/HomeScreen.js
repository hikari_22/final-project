import React from 'react';
import {View, Text, SafeAreaView, TextInput,searchText, FlatList, ListItem, StyleSheet, StatusBar} from 'react-native';
const DATA = 
  [
    {
      "id": "1",
      "nama": "Espresso",
      "harga": "25000",
    },
    {
      "id": "2",
      "nama": "Ristretto",
      "harga": "30000",
    },
    {
      "id": "3",
      "nama": "Lungo",
      "harga": "25000",
    },
    {
      "id": "4",
      "nama": "Doppio",
      "harga": "24000",
    },
    {
      "id": "5",
      "nama": "Latte",
      "harga": "34000",
    
    },
    {
      "id": "6",
      "nama": "Cappucino",
      "harga": "20000",
    },
    {
      "id": "7",
      "nama": "Con panna",
      "harga": "21000",
    },
    {
      "id": "8",
      "nama": "Kopi tubruk",
      "harga": "10000",
    },
    {
      "id": "9",
      "nama": "Cold Brew",
      "harga": "28000",
    },
    {
      "id": "10",
      "nama": "Americano",
      "harga": "31000",
    }
  ]

const Item = ({ nama }) => (
  <View style={styles.item}>
    <Text style={styles.title}>{nama}</Text>
  </View>
)
const HomeScreen = () => {
  const renderItem = ({ item }) => (
    <Item title={item.nama} />
  );
  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={{flex: 1, padding: 16}}>
        
           <TextInput
            style={{ backgroundColor: 'white', marginTop: 8 ,width : 300, height : 35, marginHorizontal : 10, borderRadius : 5 }}
            placeholder="Cari barang.."
            onChangeText={(searchText) => this.setState({ searchText })}
          />
     <FlatList
        data={DATA}
        renderItem={renderItem}
        keyExtractor={item => item.id}
      />
      </View>
      
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
});

export default HomeScreen;